create database if not exists bdInventario;
use bdInventario;

-- Creacion de tablas
CREATE TABLE IF NOT EXISTS categorias(
    idCategoria INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL
)engine InnoDB;

CREATE TABLE IF NOT EXISTS proveedores (
    idProveedor INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(40) NOT NULL,    
    correo VARCHAR(30) NOT NULL,
    telefono VARCHAR(9) NOT NULL
)engine InnoDB;

CREATE TABLE IF NOT EXISTS productos (
    idProducto INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    idCategoria INT NOT NULL,    
    stock INT NOT NULL,
    ubicacion VARCHAR(50),
    CONSTRAINT FK_cat FOREIGN KEY (idCategoria)
        REFERENCES categorias (idCategoria)
        ON DELETE CASCADE ON UPDATE CASCADE
)engine InnoDB;

CREATE TABLE IF NOT EXISTS usuarios (
    idUsuario INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(20) NOT NULL,
    cargo VARCHAR(50) NOT NULL,
    nombreUsuario VARCHAR(20) NOT NULL,
    clave VARCHAR(150) NOT NULL   
)  ENGINE INNODB;

CREATE TABLE IF NOT EXISTS ingresos (
    idIngreso INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    recibidoPor INT NOT NULL,
    idProveedor INT NOT NULL,
    fecha DATE NOT NULL,
    observacion VARCHAR(120),
    detalle INT NOT NULL,    
    CONSTRAINT FK_prov FOREIGN KEY (idProveedor)
        REFERENCES proveedores (idProveedor)
        ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FK_prod FOREIGN KEY (detalle)
        REFERENCES productos (idProducto)
        ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT FK_usu FOREIGN KEY (recibidoPor)
        REFERENCES usuarios (idUsuario)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE INNODB;

CREATE TABLE IF NOT EXISTS salidas (
    idSalida INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    entregadoPor INT NOT NULL,
    destino VARCHAR(120) NOT NULL,
    fecha DATE NOT NULL,
    detalle INT NOT NULL,
    CONSTRAINT FK_prod1 FOREIGN KEY (detalle)
        REFERENCES productos (idProducto)
        ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT FK_usu2 FOREIGN KEY (entregadoPor)
        REFERENCES usuarios (idUsuario)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE INNODB;

