<%-- 
    Document   : categoria
    Created on : 05-17-2020, 08:10:24 AM
    Author     : amand
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Categoria</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" rel="stylesheet" type="text/css"/>        
        <link href="resources/css/trans-nav.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
        <link	href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <link rel="stylesheet"	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js">
        </script>
        <style type="text/css">
            #body-row {
                margin-left: 0;
                margin-right: 0;
            }

            #sidebar-container {
                min-height: 100vh;
                background-color: #333;
                padding: 0;
            }

            /* Sidebar sizes when expanded and expanded */
            .sidebar-expanded {
                width: 230px;
            }

            .sidebar-collapsed {
                width: 60px;
            }

            /* Menu item*/
            #sidebar-container .list-group a {
                height: 50px;
                color: white;
            }

            /* Submenu item*/
            #sidebar-container .list-group .sidebar-submenu a {
                height: 45px;
                padding-left: 30px;
            }

            .sidebar-submenu {
                font-size: 0.9rem;
            }

            /* Separators */
            .sidebar-separator-title {
                background-color: #333;
                height: 35px;
            }

            .sidebar-separator {
                background-color: #333;
                height: 25px;
            }

            .logo-separator {
                background-color: #333;
                height: 60px;
            }

            /* Closed submenu icon */
            #sidebar-container .list-group .list-group-item[aria-expanded="false"] .submenu-icon::after
            {
                content: " \f0d7";
                font-family: FontAwesome;
                display: inline;
                text-align: right;
                padding-left: 10px;
            }
            /* Opened submenu icon */
            #sidebar-container .list-group .list-group-item[aria-expanded="true"] .submenu-icon::after
            {
                content: " \f0da";
                font-family: FontAwesome;
                display: inline;
                text-align: right;
                padding-left: 10px;
            }
        </style>
    </head>  
    <body>
            <nav class="navbar navbar-dark bg-primary">
			<span class="navbar-brand mb-0 "
                              style="font-family: 'Raleway', sans-serif; "><h1>El Gustito</h1></span> <span
				style="margin-left:50%;  font-family: 'Raleway', sans-serif; color: #fff;">Bienvenido "NombreUsuarioSesion"</span>
			<a class="btn" href="Usuario?action=salir" style="text-decoration: none; background-color: #000">Salir</a>	
		</nav>
		<!-- This menu is hidden in bigger devices with d-sm-none. 
           The sidebar isn't proper for smaller screens imo, so this dropdown menu can keep all the useful sidebar itens exclusively for smaller screens  -->
		<li class="nav-item dropdown d-sm-block d-md-none"><a
			class="nav-link dropdown-toggle" href="#" id="smallerscreenmenu"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Menú </a>
			<div class="dropdown-menu" aria-labelledby="smallerscreenmenu">
				<a class="dropdown-item" href="verRequisiciones">Requisiciones</a> 
				<a class="dropdown-item" href="verOrdenesCompras">Ordenes de compra</a> 
					<a class="dropdown-item" href="verProveedores">Proveedores</a>
				<a class="dropdown-item" href="verArticulos">Articulos</a>
			</div></li>
		<!-- Smaller devices menu END -->
		</ul>
	</div>
	</nav>
	<!-- NavBar END -->
	<!-- Bootstrap row -->
	<div class="row" id="body-row">
		<!-- Sidebar -->
		<div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
			<!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
			<!-- Bootstrap List Group -->
			<ul class="list-group">
				<!-- Separator with title -->
				<li
					class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
					<small>MENÚ PRINCIPAL</small>
				</li>
				
				<a href="#"
					class="bg-dark list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-start align-items-center">
						<span class="fa fa-clipboard mr-3"></span> <span
							class="menu-collapsed">Inventario</span>
					</div>
				</a>
				<a href="#"
					class="bg-dark list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-start align-items-center">
						<span class="fa fa-shopping-basket fa-fw mr-3"></span> <span
							class="menu-collapsed">Entrada</span>
					</div>
				</a>
				<a href="#"
					class="bg-dark list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-start align-items-center">
						<span class="fa fa-shopping-basket fa-fw mr-3"></span> <span
							class="menu-collapsed">Salida</span>
					</div>
				</a>
                                <a href="#"
					class="bg-dark list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-start align-items-center">
						<span class="fa fa-shopping-basket fa-fw mr-3"></span> <span
							class="menu-collapsed">Proveedores</span>
					</div>
				</a>
                                <a href="Categoria?action=consultar"
					class="bg-dark list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-start align-items-center">
						<span class="fa fa-folder-open-o mr-3"></span> <span
							class="menu-collapsed">Categoria</span>
					</div>
				</a>
				<!-- Separator without title -->
				<li class="list-group-item sidebar-separator menu-collapsed"></li>
				<!-- /END Separator -->
			</ul>
			<!-- List Group END-->
		</div>
		<!-- sidebar-container END -->
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <h3>Categorias</h3>
                </div>                
            </div>
            
            <table class="responsive-table" border="1">
                <thead class="table-dark">
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>                      
                        <th colspan="1">Operaciones</th>
                    </tr>
                </thead>
                <tbody>
                    <% int x = 1;%>
                    <c:forEach items="${consultar}" var="c">
                        <tr>
                            <td><%= x%></td>
                            <td>${c.nombre}</td>                           
                            <td><a class="btn modal-trigger bg-dark white-text" onclick="eliminar(${c.idCategoria})" > Eliminar</a>   
                                <a class="btn modal-trigger bg-dark"  href="#actualizar" onclick="actualizar(${c.idCategoria}, '${c.nombre}');"> Modificar</a>
                            </td>
                        </tr>
                        <% x++;%>
                    </c:forEach>
                </tbody>
            </table>   
            <!-- Modal Trigger Nuevo registro -->
            <a class="waves-effect  btn modal-trigger bg-dark" href="#nuevo">Nuevo Registro</a>

            <!-- Modal Structure Insertar nuevo registro -->
            <div id="nuevo" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h5>Nuevo animal</h5>                                   
                    <div class="row">
                        <form class="col s12" action="Categoria?action=insertar" method="post" >
                            <div class="row modal-form-row">
                                <div class="input-field col s8">  
                                    Nombre
                                    <input type="hidden" name="idCategoria" value=""/>
                                    <input name="nombre" id="nombre" type="text" value="" autocomplete="off">
                                </div>
                            </div>                      
                            <input class="btn modal-trigger bg-dark" type="submit"  value="Guardar"/>                                   
                        </form>
                    </div>     
                </div>
            </div>
            <!-- Modal Structure Insertar nuevo actualizar -->
            <div id="actualizar" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h5>Nueva Categoria</h5> 
                    <div class="row">
                        <form class="col s12" action="Categoria?action=actualizar" method="post" >
                            <div class="row">
                                <div class="row modal-form-row">
                                    <div class="input-field col s8">Nombre                                
                                        <input type="hidden" name="ActualizarId" id="ActualizarId"  value=""/>
                                        <input name="ActualizarNombre" id="ActualizarNombre" type="text" value="" >
                                    </div>
                                </div>
                            </div>     
                            <input class="btn modal-trigger bg-dark" type="submit"  value="Guardar"/>                                   
                        </form>
                    </div>       
                </div>
            </div>
            <script src="resources/js/jquery-3.4.1.min.js" type="text/javascript"></script>
            <script src="resources/js/materialize.js" type="text/javascript"></script>

            <script>
                //Función para que se abran los modales
                $(document).ready(function () {
                $('.modal').modal();
                });
                //Función para nav
                $(document).ready(function () {
                $(window).scroll(function () {
                if ($(window).scrollTop() > 300) {
                    $('nav').addClass('red');
                } else {
                    $('nav').removeClass('red');
                }
                });
                });
                //Función para utilizar controles select
                $(document).ready(function () {
                $('select').formSelect();
                });
                //Funcióno eliminar, envía id al controlador
                function eliminar(id) {
                var validar = confirm("Se eliminará el registro");
                if (validar === true) {
                id = parseInt(id);
                window.location.href = "Categoria?action=eliminar&idCategoria=" + id;
                }
                }
                //Funcióno actualizar, envía todos los datos al controlador
                function actualizar(id, nombre) {
                id = parseInt(id);
                console.log(id + "," + nombre);
                document.getElementById("ActualizarId").value = id;
                document.getElementById("ActualizarNombre").value = nombre;

                }
                </script>
        </div>
    </body>
</html>