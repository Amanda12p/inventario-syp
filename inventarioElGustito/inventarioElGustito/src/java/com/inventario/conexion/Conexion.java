package com.inventario.conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author amanda.pineda
 */
public class Conexion {
    private static String bd="bdInventario";
    private static String user="root";
    private static String pass="root";
    private static String url="jdbc:mysql://localhost/"+bd+"?useSSL=false";
    Connection conn=null;
    
    public Conexion(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn=DriverManager.getConnection(url,user,pass);
            if(conn!=null){
                System.out.println("Conexión exitosa.");
            }
        } catch (Exception e) {
            System.out.println("Fallo al conectar.");
        }
    }
    public Connection conectar(){
        return conn;
    }
    public void desconecatr() throws Exception{
        if(conn!=null){
            if(conn.isClosed()!=true){
                conn.close();
            }
        }        
    }
    
    public static void main(String[] args) {        
        Conexion c=new Conexion();
    }
    
}
