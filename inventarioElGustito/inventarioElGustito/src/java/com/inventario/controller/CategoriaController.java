/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.controller;

import com.inventario.conexion.Conexion;
import com.inventario.dao.CategoriaDao;
import com.inventario.model.Categoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author amand
 */
@WebServlet(name = "Categoria", urlPatterns = {"/Categoria"})
public class CategoriaController extends HttpServlet {
    
    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    List<Categoria> lista;
    Conexion cnx = new Conexion();
    
    CategoriaDao ca = new CategoriaDao(cnx);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");       
        switch (action) {
            case "consultar":
                consultar(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "ConById":
                consultarById(request, response);
                break;
        }
    }
    
    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            lista = ca.consultar();
            request.setAttribute("consultar", lista);            
            rd = request.getRequestDispatcher("View/categoria.jsp");
            rd.forward(request, response);
        } catch (Exception ex) {
            
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");        
        
        try {
            Categoria cat = new Categoria();
            cat.setNombre(nombre);
            ca.insertar(cat);

            lista = ca.consultar();
            request.setAttribute("consultar", lista);          
            rd = request.getRequestDispatcher("View/categoria.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
        
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            int idCategoria = Integer.parseInt(request.getParameter("ActualizarId"));
            String nombre = request.getParameter("ActualizarNombre");
            
            Categoria cat = new Categoria();
            cat.setIdCategoria(idCategoria);
            cat.setNombre(nombre);
            ca.actualizar(cat);            
            lista = ca.consultar();
            request.setAttribute("consultar", lista);          
            rd = request.getRequestDispatcher("View/categoria.jsp");
            rd.forward(request, response);
            
        } catch (Exception e) {
        }
    
        
    }
    
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("idCategoria"));
        try {
            ca.eliminar(id);
            lista = ca.consultar();
            request.setAttribute("consultar", lista);            
            rd = request.getRequestDispatcher("View/categoria.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
        
    }
    
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("idCategoria"));
        try {
            request.setAttribute("ConsultarById", ca.consultarById(id));
            
        } catch (Exception e) {
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
