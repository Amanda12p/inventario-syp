/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.model;

import java.util.Date;

/**
 *
 * @author amand
 */
public class Salida {
    private int idSalida;
    private Usuario entregadoPor;
    private String destino;
    private Date fecha;
    private Producto detalle;

    public int getIdSalida() {
        return idSalida;
    }

    public void setIdSalida(int idSalida) {
        this.idSalida = idSalida;
    }

    public Usuario getEntregadoPor() {
        return entregadoPor;
    }

    public void setEntregadoPor(Usuario entregadoPor) {
        this.entregadoPor = entregadoPor;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Producto getDetalle() {
        return detalle;
    }

    public void setDetalle(Producto detalle) {
        this.detalle = detalle;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + this.idSalida;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Salida other = (Salida) obj;
        if (this.idSalida != other.idSalida) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Salida{" + "idSalida=" + idSalida + ", entregadoPor=" + entregadoPor + ", destino=" + destino + ", fecha=" + fecha + ", detalle=" + detalle + '}';
    }

    
    
    
}
