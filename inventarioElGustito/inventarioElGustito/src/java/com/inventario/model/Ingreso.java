/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.model;

import java.util.Date;

/**
 *
 * @author amand
 */
public class Ingreso {
    private int idIngreso;
    private Usuario recibidoPor;
    private Proveedor Proveedor;
    private Date fecha;
    private String observacion;
    private Producto detalle;

    public int getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(int idIngreso) {
        this.idIngreso = idIngreso;
    }

    public Usuario getRecibidoPor() {
        return recibidoPor;
    }

    public void setRecibidoPor(Usuario recibidoPor) {
        this.recibidoPor = recibidoPor;
    }

    public Proveedor getProveedor() {
        return Proveedor;
    }

    public void setProveedor(Proveedor Proveedor) {
        this.Proveedor = Proveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Producto getDetalle() {
        return detalle;
    }

    public void setDetalle(Producto detalle) {
        this.detalle = detalle;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.idIngreso;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ingreso other = (Ingreso) obj;
        if (this.idIngreso != other.idIngreso) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ingreso{" + "idIngreso=" + idIngreso + ", recibidoPor=" + recibidoPor + ", Proveedor=" + Proveedor + ", fecha=" + fecha + ", observacion=" + observacion + ", detalle=" + detalle + '}';
    }

    
    
}
