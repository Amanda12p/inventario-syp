/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.dao;

import com.inventario.conexion.Conexion;
import com.inventario.model.Categoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author amand
 */
public class CategoriaDao {

    PreparedStatement stm;
    ResultSet rs;
    Conexion cnx;
    String query = "";

    public CategoriaDao(Conexion conn) {//Inicializamos en el constructor de la clase
        this.cnx = conn;
    }

    //Metodo consultar
    public ArrayList<Categoria> consultar() throws Exception {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();
        try {
            query = "Select * from categorias;";
            stm = cnx.conectar().prepareStatement(query);
            rs = stm.executeQuery();
            while (rs.next()) {
                Categoria c = new Categoria();
                c.setIdCategoria(rs.getInt("idCategoria"));
                c.setNombre(rs.getString("nombre"));
                lista.add(c);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //Metodo insertar
    public boolean insertar(Categoria c) {
        try {
            query = "INSERT INTO categorias (nombre) values(?);";
            stm = cnx.conectar().prepareStatement(query);
            stm.setString(1, c.getNombre());
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //Metodo actualizar
    public boolean actualizar(Categoria c) {
        try {
            query = "UPDATE categorias SET nombre=? WHERE idCategoria=?;";
            stm = cnx.conectar().prepareStatement(query);
            stm.setString(1, c.getNombre());
            stm.setInt(2, c.getIdCategoria());
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //Metodo consultar por id

    public ArrayList<Categoria> consultarById(int id) throws Exception {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();
        try {
            query = "Select * from categorias where idCategoria=?;";
            stm = cnx.conectar().prepareStatement(query);
            stm.setInt(1, id);
            rs = stm.executeQuery();
            while (rs.next()) {
                Categoria ca = new Categoria();
                ca.setIdCategoria(rs.getInt("idCategoria"));
                ca.setNombre(rs.getString("nombre"));
                lista.add(ca);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //Metodo consultar por id
    public boolean eliminar(int id) throws Exception {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();
        try {
            query = "delete from categorias where idCategoria=?;";
            stm = cnx.conectar().prepareStatement(query);
            stm.setInt(1, id);
            stm.executeUpdate();                        
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
